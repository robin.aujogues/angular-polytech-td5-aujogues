import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import {IPassagerDto, Passager} from "../models/passager.model";

@Injectable({
  providedIn: 'root'
})

export class PassagerService {

  constructor(private http: HttpClient) { }

  /**
   * Récupération de la liste des passagers
   */
  getPassagers(icao:string): Observable<Passager[]> {
    return this.http.get<any>(`https://randomuser.me/api?nat=fr&results=20&inc=name,email,picture&seed=${icao}`).pipe(
      map((response) => response.results.map((dto: IPassagerDto) => new Passager(dto))));
  }
}
