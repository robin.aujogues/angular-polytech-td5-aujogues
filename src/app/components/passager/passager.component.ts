import {Component, Input} from '@angular/core';
import {Passager} from "../../models/passager.model";
import {MatIcon} from "@angular/material/icon";
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [
    MatIcon,CommonModule
  ],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input() passager!: Passager;
  @Input() classe!: string;
  @Input() bagage!: number;
  @Input() afficherphoto!: boolean;

  //méthode qui retourne la couleur de la classe du passager
  getClasse(): string {
    this.classe=this.passager.classeVol;
    if(this.classe==='BUSINESS'){
      return 'color:red';
    }
    else if(this.classe==='PREMIUM'){
      return 'color:green';
    }
    else
      return 'color:blue';
  }

  //méthode qui retourne un fond rouge quand le nombres de bagages du passager est au maximum pour sa classe
  getBagage(): string {
    this.bagage=this.passager.nbBagagesSoute;
    if((this.classe==='PREMIUM' && this.bagage===3) || (this.classe==='BUSINESS' && this.bagage===2) || (this.classe==='STANDARD' && this.bagage===1)){
      return 'background-color:red';
    }
    else
      return '';
  }

}
