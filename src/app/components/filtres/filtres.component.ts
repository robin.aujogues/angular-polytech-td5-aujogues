import {
  Component,
  EventEmitter,
  Input,
  LOCALE_ID,
  OnChanges,
  Output,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import {MAT_DATE_RANGE_SELECTION_STRATEGY, MatDatepickerModule} from '@angular/material/datepicker';
import { AEROPORTS } from './../../constants/aeroport.constant';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import { IAeroport } from '../../models/aeroport.model';
import { ThreeDayRangeSelectionStrategy } from '../../date-adapter';
import {MAT_DATE_LOCALE, MatCommonModule, provideNativeDateAdapter} from '@angular/material/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {Vol} from "../../models/vol.model";
import {IFiltres} from "../../models/filtres.model";


@Component({
  selector: 'app-filtres',
  standalone: true,
  templateUrl: './filtres.component.html',
  styleUrls: ['./filtres.component.scss'],
  imports: [MatIconModule, MatButtonModule, MatInputModule,
    MatFormFieldModule, MatSelectModule, MatDatepickerModule, MatCommonModule, CommonModule, ReactiveFormsModule, FormsModule],
  providers: [
    provideNativeDateAdapter(),
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: ThreeDayRangeSelectionStrategy,
    },
  ],
  encapsulation: ViewEncapsulation.None
})
export class FiltresComponent{

  /**
   * La liste des aéroports disponibles est une constante,
   * on n'utilise que les principaux aéroports français pour l'instant
   */
  aeroports: IAeroport[] = AEROPORTS;
  aeroport: IAeroport | null = null;
  debut: Date | null = null; // La date de début sélectionnée
  fin: Date | null = null; // La date de fin sélectionnée

  @Output() filtresChanged = new EventEmitter<IFiltres>();

  // Appele cette fonction lorsque l'utilisateur clique sur le bouton "Appliquer"
  onAppliquerClick() {
    if (this.aeroport && this.debut && this.fin) {
      this.filtresChanged.emit({ aeroport: this.aeroport, debut: this.debut, fin: this.fin });
    }
  }
}
