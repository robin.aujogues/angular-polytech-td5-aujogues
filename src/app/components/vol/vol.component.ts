import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IVol, Vol} from "../../models/vol.model";
import {MatIcon} from "@angular/material/icon";
import {Passager} from "../../models/passager.model";
import {VolService} from "../../services/vol.service";
import {PassagerService} from "../../services/passager.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [
    MatIcon
  ],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {


icone!:string;
@Input() vol!: Vol;

  constructor(private activatedRoute:ActivatedRoute) {
    this.icone=this.activatedRoute.snapshot.url[0].path==='decollages'?'flight_takeoff':'flight_land'; //Permet de modifier l'icone en fonction de l'url
  }

  //Methode qui permet d'afficher le logo de la compagnie aérienne
  getLogoPath(): string {
    switch (this.vol.compagnie) {
      case 'Air France':
        return 'assets/Air France.png';
      case 'Air France Hop':
        return 'assets/Air France Hop.png';
      case 'Transavia France':
        return 'assets/Transavia France.png';
      default:
        return '';
    }
  }
}
