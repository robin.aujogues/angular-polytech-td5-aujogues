import {Component, Input} from '@angular/core';
import {Passager} from "../../models/passager.model";
import {NgForOf} from "@angular/common";
import {VolComponent} from "../vol/vol.component";
import {PassagerComponent} from "../passager/passager.component";
import {MatTooltip} from "@angular/material/tooltip";

@Component({
  selector: 'app-liste-passagers',
  standalone: true,
  imports: [
    NgForOf,
    VolComponent,
    PassagerComponent,
    MatTooltip
  ],
  templateUrl: './liste-passagers.component.html',
  styleUrls: ['./liste-passagers.component.scss']
})
export class ListePassagersComponent {
  @Input() passagers!: Passager[];
  @Input() afficherphoto!: boolean;

}
