import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IVol, Vol} from "../../models/vol.model";
import {VolComponent} from "../vol/vol.component";
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [
    VolComponent,CommonModule
  ],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {

  private lesVols!: Vol[];
@Input()
set vols(vols: Vol[]) {
  console.log(vols);
  this.lesVols = vols;
}
get vols(): Vol[] {
  return this.lesVols;
}

@Output() volSelected:EventEmitter<Vol>=new EventEmitter<Vol>();

//permet de déclencher l'événement volSelected qui permet de savoir quel vol a été sélectionné
onVolSelected(vol:Vol) {
  this.volSelected.emit(vol);
}

}
