import { Component } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import {IVol, Vol} from "../../models/vol.model";
import {VolService} from "../../services/vol.service";
import {Passager} from "../../models/passager.model";
import {PassagerService} from "../../services/passager.service";
import {CommonModule} from "@angular/common";
import {Observable} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent, CommonModule, MatSlideToggle, FormsModule],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent {

   vols!: Vol[] ;
   passagers!: Passager[];
   titre:string;
  afficherphoto: boolean = false;

  constructor(private volService: VolService,
              private passagerService:PassagerService,
              private activatedRoute:ActivatedRoute) {
    this.titre=this.activatedRoute.snapshot.url[0].path==='decollages'?'DÉCOLLAGES':'ATTERRISSAGES';//Permet de modifier le titre envoyé au .html en fonction de l'url
  } // Injectez les services nécessaires

  //Methode qui permet de récupérer les vols en fonction des filtres
  onFiltresChanged(filtres: any) {
    // Convertissez les dates de début et de fin en secondes
    const debut = Math.floor(filtres.debut.getTime() / 1000);
    const fin = Math.floor(filtres.fin.getTime() / 1000);

    // Appelle la méthode getVolsDepart ou getVolsArrivee du service VolService en fonction de l'url
    if(this.activatedRoute.snapshot.url[0].path==='decollages') {
      this.volService.getVolsDepart(filtres.aeroport.icao, debut, fin).subscribe((vols: Vol[]) => {
        console.log(vols);
        this.vols = vols;
      });
    }
    else {
      this.volService.getVolsArrivee(filtres.aeroport.icao, debut, fin).subscribe((vols: Vol[]) => {
        console.log(vols);
        this.vols = vols;
      });
    }

  }

  //Methode qui permet de récupérer les passagers en fonction du vol sélectionné
  onVolSelected(vol: Vol) {
    console.log(vol);
    // Appelle la méthode getPassager du service PassagerService
    this.passagerService.getPassagers(vol.icao).subscribe((passagers: Passager[]) => {
      console.log(passagers);
      this.passagers = passagers;

    });
  }
}
